'''
Created on Apr 21, 2017

@author: infra
'''

import os

class QFDirectoryScanner(object):
    '''
    classdocs
    '''
        
    def ScanDirectory(self, path):
        
        if(os.path.exists(path) == False):
            print "Directorio no existe"
            
        files = {}
        for onefile in os.listdir(path):
            files[onefile] = onefile
            
        return files