'''
Created on Feb 28, 2017

@author: infra
'''
from pyInfraUtils.Config import pIUConfig
from pyInfraUtils import FilesCommon
from collections import OrderedDict
c = pIUConfig()

from pyInfraUtils.LogPrinter import p

from QueueFfmpegPkg import QFFFile

import os, signal, sys

def signal_handler(signal, frame):
    p.print_info("CTRL+C detectado") 
    
    tempfiles = os.listdir(c.cfg("General", "basedir")+"temp/")
    
    if len(tempfiles) > 0:
        for file in tempfiles:
            os.remove(dirs["tempdir"]+file)

    sys.exit(0)
        
signal.signal(signal.SIGINT, signal_handler)

class QueueFfmpegMainClass():
    
    def start(self):
        p.print_debug("Main QFFLoop")

        basedir = c.cfg("General", "basedir")

        if(basedir[-1:]) != '/':
            basedir = basedir+"/"

        dirs = OrderedDict([('convdir', basedir+"conv/"),
                            ('origsdir', basedir+"conv/origs/"),
                            ('otrodir', basedir+"otros/"),
                            ('errordir', basedir+"otros/error_conv/"),
                            ('tempdir', basedir+"temp/")
                        ])

        for dirname,onedir in dirs.items():
            if os.path.isdir(onedir) == False:
                p.print_info("Creando: "+onedir)
                os.mkdir(onedir)

        files = FilesCommon.ScanDirectory(basedir)
        
        for filename, file in files.iteritems():
            if(file['isdir'] == False and file['ext'] in c.cfg("General", "source_file_extensions").split(",")):
                try:
                    qfile = QFFFile.QFFFile()
                    qfile.initialize(file)
                    qfile.convert()
                except RuntimeError as e:
                    p.print_error(e)

