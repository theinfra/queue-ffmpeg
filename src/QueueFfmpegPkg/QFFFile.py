'''
Created on Feb 28, 2017

@author: infra
'''

from pyInfraUtils.LogPrinter import p
from QueueFfmpegPkg.QueueFfmpegLauncher import c

import os

class QFFFile():
    
    properties = {}

    def p(self, name):
        if(name in self.properties):
            return self.properties[name]
        else:
            return ""

    def convert(self):
        p.print_debug(c.cfg("General", "basedir")+"conv/"+self.p("name")+".mp4")

    def initialize(self, options = {}):
        if('fullpath' not in options):
            raise RuntimeError("No se definio la ruta del archivo")

        self.properties['fullpath'] = options['fullpath']

        if('name' not in options):
            self.properties['name'] = os.path.splitext(self.fullpath)[0]

        self.properties['name'] = options['name']

    def __init__(self):
        return