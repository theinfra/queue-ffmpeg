#!/usr/bin/python2.7
import sys
sys.path.append("src")
from QueueFfmpegPkg import QueueFfmpegLauncher

app = QueueFfmpegLauncher.QueueFfmpegMainClass()

app.start()