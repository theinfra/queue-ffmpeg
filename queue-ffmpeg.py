#!/usr/bin/python2.7
import os, sys, signal, subprocess, shlex, re, pipes, string, random, datetime, ConfigParser, time
from collections import OrderedDict

Gdebug = False
Gffdebug = False
Dryrun = False
Chunksize = None #Change to 10 to make 600 to make 10 minute chunks

IIUConfigObject = ConfigParser.SafeConfigParser()

for arg in sys.argv:
	if(arg == "--debug"):
		Gdebug = True
	elif(arg == "--ffdebug"):
		Gffdebug = True
	elif(arg == "--dryrun"):
		Dryrun = True

def print_debug(msg):
	if(Gdebug == True):
		curtime = datetime.datetime.now().strftime("%H:%M:%S.%f")
		print("\033[48;5;198m\033[38;5;0m["+curtime+"][DEBUG]\033[38;5;256m\033[0m \033[48;5;5m"+msg+"\033[0m")

def print_info(msg):
	curtime = datetime.datetime.now().strftime("%H:%M:%S.%f")
	print("\033[48;5;6m\033[38;5;0m["+curtime+"][INFO]\033[38;5;256m\033[0m \033[48;5;104m"+msg+"\033[0m")	

def print_success(msg):
	curtime = datetime.datetime.now().strftime("%H:%M:%S.%f")
	print("\033[48;5;10m\033[38;5;0m["+curtime+"][SUCCESS]\033[38;5;256m\033[0m \033[48;5;64m"+msg+"\033[0m")
	
def print_error(msg):
	curtime = datetime.datetime.now().strftime("%H:%M:%S.%f")
	print("\033[48;5;9m\033[38;5;0m["+curtime+"][ERROR]\033[38;5;256m\033[0m \033[48;5;52m"+msg+"\033[0m")

class IIUConfig(object):
	'''
	classdocs
	'''
	instance = object()
	
	def parseConfigFile(self):
		global IIUConfigObject
		IIUConfigObject = ConfigParser.SafeConfigParser()
		
		configFile = os.getcwd()+'/config.ini'
		IIUConfigObject.read(configFile)
		return IIUConfigObject
	
	def cfg(self, section, key):
		val = None
		try:
			val = self.instance.get(section, key)
		except BaseException as e:
			print_error("Ocurrio un error al buscar el valor %s en la seccion de configuracion %s: %s" % (key, section, str(e)))
			return ""
			
		return val

	def __init__(self):
		 self.instance = self.parseConfigFile()

c = IIUConfig()
basedir = c.cfg("General", "basedir")

dirs = OrderedDict([('convdir', basedir+"conv/"),
					('origsdir', basedir+"conv/origs/"),
					('otrodir', basedir+"otros/"),
					('errordir', basedir+"otros/error_conv/"),
					('tempdir', basedir+"temp/")
				])
#[('a', 1), ('b', 2), ('c', 3)]

for dirname,onedir in dirs.items():
	if os.path.isdir(onedir) == False:
		print_info("Creando: "+onedir)
		if Dryrun == False:
			os.mkdir(onedir)

def signal_handler(signal, frame):
		print_info("CTRL+C detectado") 
		
		tempfiles = os.listdir(dirs["tempdir"])
		
		if len(tempfiles) > 0:
			for file in tempfiles:
				os.remove(dirs["tempdir"]+file)
		sys.exit(0)
		
signal.signal(signal.SIGINT, signal_handler)

if(Gffdebug == True):
	ffloglevel = "32"
else:
	ffloglevel = "24"

def FFconvert(file, start, length, outfile = ""):
	if(outfile == ""):
		outfile = file+".mp4"
		
	tempname = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(8))
	
	ffmpeg_cmd="ffmpeg -loglevel "+ffloglevel+" -stats -y -i \""+file+"\" -ss "+str(start)+" -t "+str(length)+" -q:a 0 -q:v 0 -f mp4 -vf fps=30 -strict -2 \""+dirs["tempdir"]+tempname+"\""# < /dev/null"
	print_debug("Convirtiendo: '"+ffmpeg_cmd+"'")
	if(Dryrun == False):
		child = subprocess.Popen(ffmpeg_cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
		streamdata = child.communicate()[0]
		exit = child.returncode
	else:
		exit = 0
		
	print_debug("Moviendo: \""+dirs["tempdir"]+tempname+"\" \""+dirs["convdir"]+outfile+"\"")
	if(Dryrun == False):
		os.rename(dirs["tempdir"]+tempname, dirs["convdir"]+outfile)
		#os.rename(basedir+file+".flv", dirs["convdir"]+file+".flv")
		
	if(exit == 255):
		print_info("Saliendo por fallo en ffmpeg")
		sys.exit(0)

for file in os.listdir(basedir):
	filename, file_extension = os.path.splitext(basedir+file)
	print_debug("Checando '%s' '%s'" % (basedir+file, file_extension))

	if os.path.isfile(basedir+file) == True and file_extension[1:] in c.cfg("General", "source_file_extensions").split(","):
		starttime = int(round(time.time() * 1000))

		print_info("Convirtiendo %s)" % (file))

		size = round(float(os.path.getsize(basedir+file)) / 1000000, 2)
		flvfile = basedir+file
		modif_date = datetime.datetime.fromtimestamp(os.path.getmtime(flvfile))
		for s in c.cfg("General", "trim_strings").split(","):
			print_debug("Reemplazando %s en %s" % (s, file))
			mp4file = file.replace(s, '')
			print_debug("Queda como %s" % (mp4file))

		mp4file = mp4file.replace(file_extension, '')
		dateformat = modif_date.strftime("%Y-%m-%d") #str(modif_date.year)+"-"+str(modif_date.month)+"-"+str(modif_date.day)
		ffprobe_cmd = ["ffprobe -v fatal -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 " + pipes.quote(basedir+file)]

		print_debug("Checando duracion: \""+ffprobe_cmd[0]+"\"")
		duration = subprocess.Popen(ffprobe_cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
		duration = duration.stdout.readline().decode('UTF-8').rstrip()
		print_debug("Duracion: "+duration)
		
		if (duration == "" or duration == "N/A" or float(duration) <= 0):
			print_error("Saltando "+filename+" por su duracion ("+duration+")")
			print_debug("Moviendo "+flvfile+" a "+dirs["errordir"]+filename+".flv")
			if(Dryrun == False):
				os.rename(flvfile, dirs["errordir"]+file)
				
			continue

		if type(Chunksize) != int or Chunksize <= 0:
			FFconvert(basedir+file, 0, duration, mp4file+".mp4")
		else:
			if float(duration) < Chunksize:
				FFconvert(basedir+file, 0, Chunksize, mp4file+".mp4")
			else:
				segment = 0
				start_time = 0
				length = Chunksize
				
				while start_time < float(duration):
					segment_left = float(duration)-((segment)*length)
					if(segment_left < 60):
						print_debug("cambio el length a 660 porque sobran "+str(segment_left)+" segundos")
						length = Chunksize + 60
					
					FFconvert(basedir+file, start_time, length, mp4file+" - S"+str(segment)+".mp4")
					
					segment += 1
					start_time += Chunksize
					
				print_success("Finalizando todos los segmentos de "+filename)
			
		print_debug("Moviendo orig: \""+basedir+file+"\" \""+dirs["convdir"]+file+"\"")
		if(Dryrun == False):
			os.rename(basedir+file, dirs["origsdir"]+file)

		endtime = int(round(time.time() * 1000))
		timetotal = (endtime - starttime)

		endseconds=(timetotal/1000)%60
		endseconds = int(endseconds)
		endminutes=(timetotal/(1000*60))%60
		endminutes = int(endminutes)
		endhours=(timetotal/(1000*60*60))%24

		print_success("Convertido %s (%s MB en %sh:%sm:%ss)" % (file, size, endhours, endminutes, endseconds))

print_success("Proceso terminado")